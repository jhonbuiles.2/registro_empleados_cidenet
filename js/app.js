var app = angular.module('CidenetPrueba',[ ]);

    
app.controller('listadoCtrl', ['$scope', function($scope){    
 
    $scope.posicion = 10;

    // $scope.personas = [
    //     {"PrimerApellido":"Bellinger","SegundoApellido":"Chaddie","PrimerNombre":"Dimsdale","OtroNombre":"Séverine","Pais":"Philippines","TipoID":"jcb","NumID":"3550138317368104","Email":"cdimsdale0@amazon.de","FechaIngreso":"9/2/2020","Area":"VP Accounting","Estado":true,"FechaRegistro":"3/21/2020"},
    //     {"PrimerApellido":"Larner","SegundoApellido":"Eachelle","PrimerNombre":"Paramore","OtroNombre":"Almérinda","Pais":"Portugal","TipoID":"mastercard","NumID":"5100130026574924","Email":"eparamore1@google.com.au","FechaIngreso":"12/17/2020","Area":"Accountant III","Estado":true,"FechaRegistro":"11/24/2020"},
    //     {"PrimerApellido":"Chin","SegundoApellido":"Stoddard","PrimerNombre":"Fassbender","OtroNombre":"Pål","Pais":"Japan","TipoID":"visa-electron","NumID":"4508443401391280","Email":"sfassbender2@nytimes.com","FechaIngreso":"12/7/2020","Area":"Nurse","Estado":true,"FechaRegistro":"10/1/2020"},
    //     {"PrimerApellido":"Bricham","SegundoApellido":"Leslie","PrimerNombre":"Bevar","OtroNombre":"Gaétane","Pais":"China","TipoID":"diners-club-carte-blanche","NumID":"30123664479098","Email":"lbevar3@google.pl","FechaIngreso":"10/4/2020","Area":"Developer I","Estado":false,"FechaRegistro":"8/9/2020"},
    //     {"PrimerApellido":"Cereceres","SegundoApellido":"Emylee","PrimerNombre":"Doust","OtroNombre":"Yè","Pais":"Croatia","TipoID":"jcb","NumID":"3542883348269554","Email":"edoust4@toplist.cz","FechaIngreso":"5/26/2020","Area":"Clinical Specialist","Estado":false,"FechaRegistro":"11/25/2020"},
    //     {"PrimerApellido":"Jedrych","SegundoApellido":"Thorsten","PrimerNombre":"Fuente","OtroNombre":"Wá","Pais":"Indonesia","TipoID":"visa-electron","NumID":"4175000771731552","Email":"tfuente5@paginegialle.it","FechaIngreso":"9/15/2020","Area":"Nuclear Power Engineer","Estado":false,"FechaRegistro":"11/13/2020"},
    //     {"PrimerApellido":"Danelet","SegundoApellido":"Caresse","PrimerNombre":"Longworth","OtroNombre":"Marie-noël","Pais":"Poland","TipoID":"maestro","NumID":"6763245584542570","Email":"clongworth6@time.com","FechaIngreso":"7/10/2020","Area":"VP Marketing","Estado":true,"FechaRegistro":"9/24/2020"},
    //     {"PrimerApellido":"Leathes","SegundoApellido":"Freddie","PrimerNombre":"Minear","OtroNombre":"Tán","Pais":"Denmark","TipoID":"china-unionpay","NumID":"5602233280202065921","Email":"fminear7@topsy.com","FechaIngreso":"5/30/2020","Area":"Statistician I","Estado":false,"FechaRegistro":"12/24/2020"},
    //     {"PrimerApellido":"Sprouls","SegundoApellido":"Oneida","PrimerNombre":"Sutherel","OtroNombre":"Mà","Pais":"Indonesia","TipoID":"jcb","NumID":"3585813360487899","Email":"osutherel8@zdnet.com","FechaIngreso":"8/3/2020","Area":"Marketing Assistant","Estado":true,"FechaRegistro":"5/16/2020"},
    //     {"PrimerApellido":"Trill","SegundoApellido":"Kent","PrimerNombre":"Ness","OtroNombre":"Clélia","Pais":"Brazil","TipoID":"jcb","NumID":"3573709330670529","Email":"kness9@google.it","FechaIngreso":"11/25/2020","Area":"Structural Engineer","Estado":false,"FechaRegistro":"5/2/2020"},
    //     {"PrimerApellido":"Hanselmann","SegundoApellido":"Gal","PrimerNombre":"Duggon","OtroNombre":"Görel","Pais":"Indonesia","TipoID":"jcb","NumID":"3583727570269610","Email":"gduggona@tiny.cc","FechaIngreso":"7/28/2020","Area":"Social Worker","Estado":true,"FechaRegistro":"5/19/2020"},
    //     {"PrimerApellido":"Tavernor","SegundoApellido":"Albina","PrimerNombre":"Blackmuir","OtroNombre":"Lyséa","Pais":"France","TipoID":"china-unionpay","NumID":"5602250021700705","Email":"ablackmuirb@mail.ru","FechaIngreso":"11/12/2020","Area":"Software Test Engineer I","Estado":false,"FechaRegistro":"7/16/2020"},
    //     {"PrimerApellido":"Fremantle","SegundoApellido":"Yvonne","PrimerNombre":"Tresvina","OtroNombre":"Gösta","Pais":"China","TipoID":"mastercard","NumID":"5100132671611118","Email":"ytresvinac@desdev.cn","FechaIngreso":"9/16/2020","Area":"Account Coordinator","Estado":false,"FechaRegistro":"8/18/2020"},
    //     {"PrimerApellido":"Arblaster","SegundoApellido":"Obadias","PrimerNombre":"Satyford","OtroNombre":"Ruò","Pais":"Indonesia","TipoID":"jcb","NumID":"3548006147699557","Email":"osatyfordd@clickbank.net","FechaIngreso":"2/16/2020","Area":"Executive Secretary","Estado":true,"FechaRegistro":"5/19/2020"},
    //     {"PrimerApellido":"Ourtic","SegundoApellido":"Quinta","PrimerNombre":"Glanders","OtroNombre":"Publicité","Pais":"China","TipoID":"jcb","NumID":"3586758921804257","Email":"qglanderse@japanpost.jp","FechaIngreso":"8/21/2020","Area":"Staff Accountant III","Estado":false,"FechaRegistro":"11/25/2020"},
    //     {"PrimerApellido":"Clavey","SegundoApellido":"Nadiya","PrimerNombre":"Redford","OtroNombre":"Mårten","Pais":"United States","TipoID":"visa-electron","NumID":"4917006574768371","Email":"nredfordf@nasa.gov","FechaIngreso":"7/15/2020","Area":"Chemical Engineer","Estado":false,"FechaRegistro":"3/10/2020"},
    //     {"PrimerApellido":"Howen","SegundoApellido":"Rossie","PrimerNombre":"Adne","OtroNombre":"Edmée","Pais":"Mali","TipoID":"jcb","NumID":"3577320694414334","Email":"radneg@taobao.com","FechaIngreso":"5/29/2020","Area":"Marketing Assistant","Estado":true,"FechaRegistro":"6/25/2020"},
    //     {"PrimerApellido":"Shoulder","SegundoApellido":"Christal","PrimerNombre":"Brimman","OtroNombre":"Mélissandre","Pais":"Greece","TipoID":"visa","NumID":"4668319369775662","Email":"cbrimmanh@about.me","FechaIngreso":"4/25/2020","Area":"Assistant Professor","Estado":false,"FechaRegistro":"1/18/2020"},
    //     {"PrimerApellido":"Daviddi","SegundoApellido":"Salome","PrimerNombre":"Hargey","OtroNombre":"Marlène","Pais":"Portugal","TipoID":"bankcard","NumID":"5602213818745721","Email":"shargeyi@nih.gov","FechaIngreso":"11/2/2020","Area":"Web Designer I","Estado":true,"FechaRegistro":"9/10/2020"},
    //     {"PrimerApellido":"Petrolli","SegundoApellido":"Hanni","PrimerNombre":"Lippini","OtroNombre":"Véronique","Pais":"New Caledonia","TipoID":"visa-electron","NumID":"4913539553886024","Email":"hlippinij@ocn.ne.jp","FechaIngreso":"4/21/2020","Area":"Software Test Engineer IV","Estado":true,"FechaRegistro":"11/22/2020"},
    //     {"PrimerApellido":"Drogan","SegundoApellido":"Helaine","PrimerNombre":"Ive","OtroNombre":"Réjane","Pais":"Mexico","TipoID":"jcb","NumID":"3587415612425327","Email":"hivek@sphinn.com","FechaIngreso":"4/7/2020","Area":"Structural Analysis Engineer","Estado":false,"FechaRegistro":"11/4/2020"},
    //     {"PrimerApellido":"Newdick","SegundoApellido":"Yancy","PrimerNombre":"Muccino","OtroNombre":"Aloïs","Pais":"Colombia","TipoID":"americanexpress","NumID":"343217224548848","Email":"ymuccinol@theglobeandmail.com","FechaIngreso":"9/5/2020","Area":"Occupational Therapist","Estado":true,"FechaRegistro":"3/30/2020"},
    //     {"PrimerApellido":"Leopard","SegundoApellido":"Brig","PrimerNombre":"Twinberrow","OtroNombre":"Cloé","Pais":"China","TipoID":"switch","NumID":"4903997258752556","Email":"btwinberrowm@quantcast.com","FechaIngreso":"1/12/2020","Area":"Editor","Estado":true,"FechaRegistro":"9/23/2020"},
    //     {"PrimerApellido":"Layman","SegundoApellido":"Florrie","PrimerNombre":"Dooland","OtroNombre":"Pò","Pais":"Poland","TipoID":"jcb","NumID":"3583507413833768","Email":"fdoolandn@marriott.com","FechaIngreso":"3/31/2020","Area":"Nurse Practicioner","Estado":true,"FechaRegistro":"12/13/2020"},
    //     {"PrimerApellido":"Wheaton","SegundoApellido":"Vittorio","PrimerNombre":"Birkin","OtroNombre":"Styrbjörn","Pais":"Ukraine","TipoID":"diners-club-enroute","NumID":"201833298809014","Email":"vbirkino@mtv.com","FechaIngreso":"4/9/2020","Area":"Staff Scientist","Estado":false,"FechaRegistro":"5/17/2020"},
    //     {"PrimerApellido":"Bartholomieu","SegundoApellido":"Salomon","PrimerNombre":"Palliser","OtroNombre":"Michèle","Pais":"Portugal","TipoID":"mastercard","NumID":"5100132827156935","Email":"spalliserp@marketwatch.com","FechaIngreso":"9/3/2020","Area":"Human Resources Manager","Estado":true,"FechaRegistro":"1/23/2020"},
    //     {"PrimerApellido":"Spikings","SegundoApellido":"Glenn","PrimerNombre":"Belison","OtroNombre":"Marie-josée","Pais":"China","TipoID":"diners-club-us-ca","NumID":"5458414348734731","Email":"gbelisonq@goo.gl","FechaIngreso":"12/31/2020","Area":"Media Manager I","Estado":true,"FechaRegistro":"7/16/2020"},
    //     {"PrimerApellido":"Tosspell","SegundoApellido":"Constancia","PrimerNombre":"Handsheart","OtroNombre":"Mégane","Pais":"Philippines","TipoID":"jcb","NumID":"3558581913777016","Email":"chandsheartr@forbes.com","FechaIngreso":"4/26/2020","Area":"Clinical Specialist","Estado":false,"FechaRegistro":"10/12/2020"},
    //     {"PrimerApellido":"Lendon","SegundoApellido":"Orton","PrimerNombre":"Dodell","OtroNombre":"Åslög","Pais":"Portugal","TipoID":"jcb","NumID":"3533908372480545","Email":"ododells@indiatimes.com","FechaIngreso":"12/26/2020","Area":"VP Product Management","Estado":false,"FechaRegistro":"9/8/2020"},
    //     {"PrimerApellido":"Eckh","SegundoApellido":"Reggis","PrimerNombre":"Llewellen","OtroNombre":"Françoise","Pais":"Indonesia","TipoID":"mastercard","NumID":"5002352792646830","Email":"rllewellent@ucoz.com","FechaIngreso":"9/3/2020","Area":"Analyst Programmer","Estado":true,"FechaRegistro":"10/21/2020"}

    //     ];
    //     debugger;
    //     var empleadosJson = $scope.personas;

        $scope.siguiente = function(){

            if ($scope.personas.length > $scope.posicion ) {

                $scope.posicion += 10;
            }

        }

        $scope.anterior = function(){

            if ($scope.posicion > 10 ) {

                $scope.posicion -= 10;
            }

        }

        $scope.FechaActual = function(){

            $scope.CurrentDate = new Date();

        } 

        $scope.formVisibilityReg = false;
        $scope.formVisibilityEdit = false;

        $scope.ShowFormReg = function(){

            $scope.formVisibilityReg = true;
            console.log($scope.formVisibilityReg);
            $scope.ShowBtnCrear();

        } 

        $scope.ShowFormEdit = function(){

            $scope.formVisibilityEdit  = true;
            console.log($scope.formVisibilityEdit);
            $scope.ShowBtnCrear();

        }

        $scope.ShowBtnCrear = function(){
            if($scope.formVisibilityReg === true || $scope.formVisibilityEdit === true){
                $scope.formVisibility  = true;      
            }
            console.log($scope.formVisibility)

        }  

        //Crear instancia Base de datos
        var db = new cls_db();
        db.conexion_global();
        $scope.date = new Date();
        db.crear_db();

        // Guardar informacion offline
        $scope.SaveInfo = function () {  

            var PrimerApellido   = document.getElementById("PrimerApellido").value;
            var SegundoApellido = document.getElementById("SegundoApellido").value;
            var PrimerNombre = document.getElementById("PrimerNombre").value;
            var OtroNombre = document.getElementById("OtroNombre").value;
            var Pais = document.getElementById("Pais");
            Pais = Pais.options[Pais.selectedIndex].text;
            var TipoID = document.getElementById("TipoID");
            TipoID = TipoID.options[TipoID.selectedIndex].text;
            var NumID  = document.getElementById("NumID").value;
            var Email  = document.getElementById("Email").value;
            var FechIng  = document.getElementById("FechIng").value;
            var Area = document.getElementById("Area");
            Area = Area.options[Area.selectedIndex].text;
            var Estado = document.getElementById("Estado");
            Estado = Estado.options[Estado.selectedIndex].text;
            var FechReg = document.getElementById("FechReg").value;

            $scope.formVisibility = false;
            console.log($scope.formVisibility)

            var flag = $scope.validateCampo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg);

            debugger;
            if (flag) {

                //Creacion de registros en WebSQL
                db.insertarDatosInfo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg);
            }
            else{

                console.log("Error!", "Formulario incompleto.");
            }
            
        }

        // Guardar informacion offline
        $scope.SaveInfoEdit = function () {  
            debugger;
            var PrimerApellido   = document.getElementById("PrimerApellidoEdit").value;
            var SegundoApellido = document.getElementById("SegundoApellidoEdit").value;
            var PrimerNombre = document.getElementById("PrimerNombreEdit").value;
            var OtroNombre = document.getElementById("OtroNombreEdit").value;
            var Pais = document.getElementById("PaisEdit");
            Pais = Pais.options[Pais.selectedIndex].text;
            var TipoID = document.getElementById("TipoIDEdit");
            TipoID = TipoID.options[TipoID.selectedIndex].text;
            var NumID  = document.getElementById("NumIDEdit").value;
            var Email  = document.getElementById("EmailEdit").value;
            var FechIng  = document.getElementById("FechIngEdit").value;
            var Area = document.getElementById("AreaEdit");
            Area = Area.options[Area.selectedIndex].text;
            var Estado = document.getElementById("EstadoEdit");
            Estado = Estado.options[Estado.selectedIndex].text;
            var FechReg = document.getElementById("FechRegEdit").value;

            $scope.formVisibility = false;
            console.log($scope.formVisibility)

            var flag = $scope.validateCampo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg);

            
            if (flag) {

                //Creacion de registros en WebSQL
                db.insertarDatosInfo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg);
            }
            else{

                console.log("Error!", "Formulario incompleto.");
            }
            
        }

        $scope.validateCampo = function(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg){

            var flag = true;

            if (PrimerApellido === "") {
                flag = false;
            }else if (SegundoApellido === "") {
                flag = false;
            }else if(PrimerNombre === ""){
                flag = false;
            }else if(OtroNombre === ""){
                flag = false;
            }else if(Pais === ""){
                flag = false;
            }
            else if(TipoID === ""){
                flag = false;
            }
            else if(NumID === ""){
                flag = false;
            }
            else if(Email === ""){
                flag = false;
            }
            else if(FechIng === ""){
                flag = false;
            }
            else if(Area === ""){
                flag = false;
            }
            else if(Estado === ""){
                flag = false;
            }

            return flag;
        }

        //Consulta de registros en WebSQL
        $scope.getinfo = function(){
            //Obtenemos informacion de la base datos local.
            db.GetDataInfo(function (datos) {
                if (datos.rows != null) {
                    // Datos de cabecera
                    console.log('Guardando datos de informacion final.');
                    $scope.SaveDataInfo(datos.rows);
                }
            });                
        }

        //Editar de registros en WebSQL
        $scope.Editar = function(persona) {
            
            $scope.ShowFormEdit();
            console.log(persona);
            var informacion = [];
            var info = {};

            info['id']              = persona.id
            info['PrimerApellido']  = persona.PrimerApellido
            info['SegundoApellido'] = persona.SegundoApellido
            info['PrimerNombre']    = persona.PrimerNombre
            info['OtroNombre']      = persona.OtroNombre
            info['Pais']            = persona.Pais
            info['TipoID']          = persona.TipoID
            info['NumID']           = persona.NumID
            info['Email']           = persona.Email
            info['FechIng']         = persona.FechIng
            info['Area']            = persona.Area
            info['Estado']          = persona.Estado
            informacion.push(info);
            info = {};
            
            debugger;          
            // if(persona.Pais === "Estados Unidos"){
            //    document.getElementById("PaisEdit").value('Estados Unidos', 2); 
            // }

            $scope.registros = informacion;
            $scope.$apply(); //Aplica obligatoriamente los cambios en el refresh de la pagina

            
        }

        //Llevar registros al DOM
        $scope.SaveDataInfo = function(data){
            //Armar objeto y enviar.
            var informacion = [];
            var info = {};
            for (var i = data.length - 1; i >= 0; i--) {
                info['id']              = data[i].id
                info['PrimerApellido']  = data[i].PrimerApellido
                info['SegundoApellido'] = data[i].SegundoApellido
                info['PrimerNombre']    = data[i].PrimerNombre
                info['OtroNombre']      = data[i].OtroNombre
                info['Pais']            = data[i].Pais
                info['TipoID']          = data[i].TipoID
                info['NumID']           = data[i].NumID
                info['Email']           = data[i].Email
                info['FechIng']         = data[i].FechIng
                info['Area']            = data[i].Area
                info['Estado']          = data[i].Estado
                info['FechReg']         = data[i].fecha
                informacion.push(info);
                info = {};
            }

            //Validamos informacion
            if (informacion.length > 0) {

                $scope.personas = informacion;
                $scope.$apply(); //Aplica obligatoriamente los cambios en el refresh de la pagina
                $scope.validateDataTable();

            }
        }



        //Eliminacion de registros en WebSQL
        $scope.Eliminar = function(persona) {
            debugger;
            console.log(persona.id);
            var id = persona.id;
            db.DeleteDataReg(id);
        }
      
        //Funciona para validar si la tabla a mapear es DataTable y no presentar errores al usurio
        $scope.validateDataTable = function() {
             //Validamos si la tabla ya es DataTable para destuirla y reiniciarla.
             if ($.fn.DataTable.isDataTable('#tblReg')) {
                 //Destruimos dataTable
                 $('#tblReg').DataTable().destroy();
                 //Iniciamos nuevamente la configuracion DataTable
                 $scope.configDatatable();
             } else {
                 //Iniciamos configuracion
                 $scope.configDatatable();
             }
         }

        $scope.configDatatable = function() {
            $(document).ready(function() {
                $('#tblReg').DataTable({
                    "bFilter": true,
                    "dom": 'Bfrtip',
                    "buttons": ['excel'],
                    "scrollY": "600px", //Tamaño de sroll
                    "scrollCollapse": true, //Activamos el Scroll lateral
                    "scrollX": true, //Activamos el Scrol inferior

                    "lengthMenu": [
                    [5, 25, 50, -1],
                    [5, 25, 50, "Todos"]
                    ],
                    "language": { //Configuracion de lenguaje
                    "lengthMenu": "", //Cantidad de registros a mostrar
                    "zeroRecords": "No se encontraron registros relacionados", //Texto de busqueda
                    "info": "Tabla consulta empleados", //Informacion de la paginacion
                    "infoEmpty": "No se encuentran registros disponibles", //
                    "infoFiltered": "(Se realizo busqueda en MAX registros)", //Informacion de busqueda, si no se encuentran registros
                    "searching": true,
                    "search": "Busqueda",
                    "paging": true,
                    "paginate": { //Configuracion de botones y paginacion
                    "next": "Siguiente", //Boton Siguiente
                    "previous": "Anterior" //Boton Anterior
                    },
                    }
                });
            });
        }

        $scope.getinfo(); 


}]);









